$(document).ready(function(){
    var Apikey = "AIzaSyDPs0Xq8QomEbnhe6TRartbIUnhy9L1LXk"
    var video = ''
    
    $("#form").submit(function(event){
        event.preventDefault()
        var search = $("#search").val()
        videoSearch(Apikey,search,20 )
    })
    
    function videoSearch(key, search, maxResults){
        $("#videos").empty()
        
        $.get("https://www.googleapis.com/youtube/v3/search?key=" +key +"&type=video&part=snippet&maxResults=" +maxResults +"&q="+search, function(data){
            console.log(data)

            data.items.forEach(item => {
                video = `
                <iframe width="480" height="360" src="https://www.youtube.com/embed/${item.id.videoId}" frameborder="0" allowfullscreen></iframe>
                `
            
                $("#videos").append(video)
                
            })
        })
    } 
})

